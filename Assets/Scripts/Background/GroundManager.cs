﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GroundManager : MonoBehaviour {

    List<Ground> groundPool; 

    public float speed = 1.5f;

    public bool deactiveObjectsOnStart;

    public float minSeconds = 3;
    public float maxSeconds = 6;

    float lastTime;
    float nextTime;

    void Awake()
    {
        if(groundPool == null)
            groundPool = new List<Ground>();
    }

    void Start()
    {
        if(deactiveObjectsOnStart)
        {
            for (int i = 0; i < groundPool.Count; i++)
                groundPool[i].gameObject.SetActive(false);
        }
        else
        {
            groundPool = new List<Ground>();
        }
        lastTime = 0;
        SetNextTime();
    }

    void Update()
    {
        if (groundPool.Count > 0)
        {
            if (Time.time > nextTime)
            {
                lastTime = Time.time;
                int rand = Random.Range(0, groundPool.Count);
                groundPool[rand].Activate();
                SetNextTime();
            }
        }
    }

    void SetNextTime()
    {
        nextTime = lastTime + Random.Range(minSeconds, maxSeconds);
    }

    public void AddToPool(Ground toAdd)
    {
        if (groundPool == null)
            groundPool = new List<Ground>();
        groundPool.Add(toAdd);
    }

    public void RemoveFromPool(Ground toRemove)
    {
        groundPool.Remove(toRemove);
    }
}
