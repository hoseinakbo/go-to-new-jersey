using UnityEngine;
using System.Collections;

public class CameraInfo : MonoBehaviour
{
    Camera cam;
    Transform cameraTransform;

    float halfWidth, halfHeight, width, height;

    public Rect CameraBounds
    {
        get { return new Rect(cameraTransform.position.x - halfWidth, cameraTransform.position.y - halfHeight, width, height); }
    }

    void Awake()
    {
        cam = GetComponent<Camera>();
        cameraTransform = transform;
        GenerateInfo();
    }

    void GenerateInfo()
    {
        halfHeight = cam.orthographicSize;
        halfWidth = cam.aspect * halfHeight;
        width = halfWidth * 2;
        height = halfHeight * 2;
    }

    public void Refresh()
    {
        GenerateInfo();
    }
}