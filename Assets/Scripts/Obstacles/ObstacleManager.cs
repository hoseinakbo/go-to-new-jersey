﻿using UnityEngine;
using System.Collections;

public class ObstacleManager : MonoBehaviour {

    public float endTimeAfterLastObj = 5;
    public ObstacleObject[] obstacles;

    float lastTime;
    int currentObstacle;

    bool generationObstacles;


	void Start () {
        lastTime = 0;
        currentObstacle = 0;
        generationObstacles = true;
	}
	
	void Update ()
    {
        if (generationObstacles)
        {
            if (currentObstacle == obstacles.Length)
            {
                if (Time.time > lastTime + endTimeAfterLastObj)
                {
                    generationObstacles = false;
                    GenerateEndingObstacle();
                }
            }
            else
            {
                if (Time.time > lastTime + obstacles[currentObstacle].timeAfterLastObj)
                {
                    lastTime = Time.time;
                    GenerateObstacle(obstacles[currentObstacle].type);
                    currentObstacle++;
                }
            }
        }
	}

    void GenerateObstacle(ObstacleType obsType)
    {
        Debug.Log("Generate " + obsType);
       // if(obsType == ObstacleType.Rock)
            
    }

    void GenerateEndingObstacle()
    {
        Debug.Log("Generate Ending");
    }
}

[System.Serializable]
public class ObstacleObject
{
    public ObstacleType type;
    public float timeAfterLastObj;
}

public enum ObstacleType
{
    Rock,
    Bush
}